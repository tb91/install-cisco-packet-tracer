# Installing Cisco Packet Tracer #

In the Windows 10 operating system, visit the link below to install the Packet Tracer, which is used to prepare for the Cisco exams.

Link: **[Install Packet Tracer](https://www.sysnettechsolutions.com/en/ciscopackettracer/install-cisco-packet-tracer-7-2/)**